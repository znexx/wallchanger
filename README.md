## Summary ##
**wallchanger.py** is a python script for automatically changing your desktop background, while also moving your conky system monitor to any coordinate of your specification.

## Screenshots ##
Here is the example configuration on my system:

![screenshot1_thumb.png](https://bitbucket.org/repo/npLqqk/images/676691932-screenshot1_thumb.png)

![screenshot2_thumb.png](https://bitbucket.org/repo/npLqqk/images/3748767998-screenshot2_thumb.png)