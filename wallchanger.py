#!/usr/bin/python3
import getopt, os, re, sys, time, toml
VERSION = '0.1'
CONFIG_PATH = os.path.expanduser('~/.wallchanger/config.toml')
VERBOSE = False

def find_lua_value(line, key):
	return re.search(r'^.*?=\s*(.*?)\s*(--.*)?$', line).group(1)

def find_line(lines, key):
	return [(index, line) for index, line in enumerate(lines) if key in line][0]

def update_lua_value(lines, key, new_value):
	(index, line) = find_line(lines, key)
	old_value = find_lua_value(line, key)
	lines[index] = line.replace(old_value, new_value, 1)
	return lines

def change_coords(conky_path, x, y):
	config = open(conky_path, 'r').read().split('\n')
	config = update_lua_value(config, "gap_x", str(x)+',')
	config = update_lua_value(config, "gap_y", str(y)+',')
	config = '\n'.join(config)
	open(conky_path, 'w').write(config)

def call_feh(scaling, xinerama, path):
	args = []
	args.append('feh')
	args.append('' if xinerama else ' --no-xinerama')
	args.append(' --bg-'+scaling)
	args.append(' '+path)
	args = ''.join(args)
	if VERBOSE:
		print(args)
	os.system(args)

def parse_arguments(argv):
	global VERBOSE, CONFIG_PATH
	try:
		opts, args = getopt.getopt(argv[1:], 'c:hvV', ['config=', 'help', 'verbose', 'version'])
	except getopt.GetoptError as err:
		print(str(err))
		print_help(argv[0])
	for o, a in opts:
		if o in ('-v', '--verbose'):
			VERBOSE = True
		elif o in ('-c', '--config'):
			CONFIG_PATH = os.path.expanduser(a)
		elif o in ('-h', '--help'):
			print_help(argv[0])
			sys.exit(2)
		elif o in ('-V', '--version'):
			print('wallchanger.py v'+VERSION)
			sys.exit(2)

def read_config():
	config = toml.loads(open(CONFIG_PATH).read())
	config.setdefault('conky_config', '~/.config/conky/conky.conf')
	config.setdefault('wallpaper_dir', '/usr/share/backgrounds/')
	config.setdefault('background', {})
	config.setdefault('scaling', 'fill')
	config.setdefault('delay', 300)
	config.setdefault('xinerama', True)
	return config

def print_config(config):
	width = max([len(l) for l in config.keys()])
	for key in sorted(config):
		print(key.ljust(width)+'= ', end='')
		print(config[key])

def print_help(bin_name):
	print('Usage: '+bin_name)
	print('An automatic wallpaper switcher and conky positioner')
	print(' -c  --config=file  Use file as config')
	print(' -h  --help         Show this help')
	print(' -V  --version      Show version number')
	print(' -v  --verbose      More verbose output')

def read_config_value(config, background, key):
	if key in background:
		return background[key]
	elif key in config:
		return config[key]
	else:
		return ''

if __name__ == '__main__':
	parse_arguments(sys.argv)
	config = read_config()
	if VERBOSE:
		print_config(config)
	while True:
		for b in config['background']:
			dir_name	= read_config_value(config, b, 'wallpaper_dir')
			dir_name	= os.path.expanduser(dir_name)
			file_name	= read_config_value(config, b, 'file')
			if type(file_name) is list:
				path	= ' '.join([dir_name+'/'+f for f in file_name])
			else:
				path	= dir_name+'/'+file_name
			x		= read_config_value(config, b, 'x')
			y		= read_config_value(config, b, 'y')
			scaling		= read_config_value(config, b, 'scaling')
			conky_config	= read_config_value(config, b, 'conky_config')
			conky_config	= os.path.expanduser(conky_config)
			xinerama	= read_config_value(config, b, 'xinerama')
			delay		= read_config_value(config, b, 'delay')

			change_coords(conky_config, x, y)
			call_feh(scaling, xinerama, path)
			time.sleep(delay)
